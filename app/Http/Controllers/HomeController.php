<?php

namespace App\Http\Controllers;

use App\Jobs\SendWelcomeEmail;
use App\Mail\WelcomeEmail;

class HomeController extends Controller
{
    public function index()
    {
        $email = new WelcomeEmail();
        $this->dispatch($email);
        return view('welcome');
    }
}
